# miniconnect

[![](https://images.microbadger.com/badges/image/homesmarthome/miniconnected.svg)](https://microbadger.com/images/homesmarthome/miniconnected)

[![](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fregistry.hub.docker.com%2Fv2%2Frepositories%2Fhomesmarthome%miniconnected%2Ftags&query=%24.results%5B1%5D.name&colorB=4286f4)]("https://hub.docker.com/r/homesmarthome/miniconnected/")

Gets information from Mini Connect and sends the JSON string via mqtt.

 Mini Connect credntials have to be provided as environmanet variables.