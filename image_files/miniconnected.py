import paho.mqtt.client as mqtt
import os
import json

import requests

from bimmer_connected.account import ConnectedDriveAccount
from bimmer_connected.country_selector import get_region_from_name

MQTT_HOST = os.environ['MQTT_HOST']
MINI_CONNECTED_USER = os.environ['MINI_CONNECTED_USER']
MINI_CONNECTED_PASS = os.environ['MINI_CONNECTED_PASS']
MINI_CONNECTED_REGION = 'rest_of_world'

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("/miniconnected/get/status")

def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))
    if (msg.topic == "/miniconnected/get/status"):
        print("message received: /miniconnected/get/status")
        payload = get_status(MINI_CONNECTED_USER, MINI_CONNECTED_PASS, MINI_CONNECTED_REGION)

        client.publish("/miniconnected/status", json.dumps(payload))

def get_status(username, password, region):
    """Get the vehicle status."""
    account = ConnectedDriveAccount(username, password, get_region_from_name(region))
    account.update_vehicle_states()

    return account.vehicles[0].state.attributes

if __name__ == "__main__":
    client = mqtt.Client('miniconnected')
    client.connect(MQTT_HOST, 1883, 60)
    client.on_connect = on_connect
    client.on_message = on_message

    client.loop_forever()